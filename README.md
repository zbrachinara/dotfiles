A collection of dotfiles I'm building for a hyprland-based desktop.

Missing from this repository are configs for sddm and grub. sddm needs to be set up to call `.local/bin/hyprland-from-dm`, and on thinkpad t440, the file at `/etc/default/grub` needs to be modified to pass in the linux argument `psmouse.synaptics_intertouch=0` (this should be appended to the variable `GRUB_CMDLINE_LINUX_DEFAULT`
